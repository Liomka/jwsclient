import Observers.ConnectionWindowEvent;
import Observers.IConnectionWindowListener;
import Observers.IMainMenubarListener;
import Observers.MainMenubarEvent;

import javax.swing.*;
import javax.websocket.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.URI;


/**
 * Created by aagombar on 27/03/14.
 */
public class MainWindow extends JFrame {

    private Session _session;

    /*
    private Client _client;
    */
    public MainWindow() {
        // WINDOW POSITION
        int width = 800;
        int height = 600;

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        Rectangle windowPosition = new Rectangle(width, height);
        windowPosition.setLocation((Toolkit.getDefaultToolkit().getScreenSize().width  / 2) - (width  / 2),
                (Toolkit.getDefaultToolkit().getScreenSize().height / 2) - (height / 2));
        setBounds(windowPosition);

        setTitle("Client");

        // WINDOW CONTENT
        setJMenuBar(menubar());
        getContentPane().setLayout(new BorderLayout());

        getContentPane().add(messageLog(), BorderLayout.CENTER);
        getContentPane().add(userList(), BorderLayout.EAST);
        getContentPane().add(userInput(), BorderLayout.SOUTH);

        // CONNECTIONS
        addWindowListener(new MainWindowListener());

        disconnectFromServer();
    }

    private void connectToServer(String  ip, int port, String name) {

        String destUri = "ws://"+ ip;
        WebSocketContainer container = ContainerProvider.getWebSocketContainer();

        try {
            _session = container.connectToServer(WebSocketClient.class, URI.create(destUri));
            _session.addMessageHandler(new MessageHandler.Whole<String>() {
                @Override
                public void onMessage(String message) {
                    messageLog().append(message + "\n");
                }
            });
            // SetName
            userInput().setEnabled(true);
            userList().setEnabled(true);
            messageLog().setEnabled(true);
        } catch (DeploymentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void disconnectFromServer() {
        if (_session != null) {
            try {
                _session.close();
                _session = null;
                userInput().setEnabled(false);
                userList().setEnabled(false);
                messageLog().setEnabled(false);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // GUI
    private MainMenuBar _menubar;
    private ConnectionWindow _connectionWindow;
    private JList<String> _userList;
    private JTextArea _messageLog;
    private JTextField _userInput;

    private MainMenuBar menubar() {
        if (_menubar == null) {
            _menubar = new MainMenuBar();
            _menubar.addItemsListener(new IMainMenubarListener() {
                @Override
                public void menuClicked(MainMenubarEvent mainMenubarEvent) {
                    String actionCommand = ((JMenuItem)mainMenubarEvent.getSource()).getActionCommand();
                    if (actionCommand.compareToIgnoreCase("connectToServer") == 0)
                        connectionWindow().setVisible(true);
                    else if (actionCommand.compareToIgnoreCase("disconnectFromServer") == 0)
                        disconnectFromServer();
                    else if (actionCommand.compareToIgnoreCase("quit") == 0) {
                        dispose();
                    }
                }
            });
        }
        return _menubar;
    }

    private ConnectionWindow connectionWindow() {
        if (_connectionWindow == null) {
            _connectionWindow = new ConnectionWindow();
            _connectionWindow.addConnectionWindowListener(new IConnectionWindowListener() {
                @Override
                public void connect(ConnectionWindowEvent connectionWindowEvent, String ip, int port, String name) {
                    connectToServer(ip, port, name);
                }

                @Override
                public void abort(ConnectionWindowEvent connectionWindowEvent) {}
            });
        }
        return _connectionWindow;
    }

    private JList<String> userList() {
        if (_userList == null) {
            _userList = new JList();
        }
        return _userList;
    }

    private JTextArea messageLog() {
        if (_messageLog == null) {
            _messageLog = new JTextArea();
            _messageLog.setEditable(false);
        }
        return _messageLog;
    }

    private JTextField userInput() {
        if (_userInput == null) {
            _userInput = new JTextField();
            _userInput.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        _session.getBasicRemote().sendText(e.getActionCommand());
                        userInput().setText("");
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            });
        }
        return _userInput;
    }

    private class MainWindowListener extends WindowAdapter {
        @Override
        public void windowOpened(WindowEvent e) {
            userInput().requestFocus();
        }
    }

    public static void main(String[] args) {
        new MainWindow().setVisible(true);
    }

}
