package Observers;

import java.util.EventListener;

/**
 * Created by aagombar on 27/03/14.
 */
public interface IConnectionWindowListener extends EventListener {

    public void connect(ConnectionWindowEvent connectionWindowEvent, String ip, int port, String name);
    public void abort(ConnectionWindowEvent connectionWindowEvent);
}
