package Observers;

import java.util.EventListener;

/**
 * Created by aagombar on 24/03/14.
 */
public interface IMainMenubarListener extends EventListener {

    public void menuClicked(MainMenubarEvent mainMenubarEvent);
}
