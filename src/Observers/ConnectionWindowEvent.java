package Observers;

import javax.swing.*;
import java.util.EventObject;

/**
 * Created by aagombar on 27/03/14.
 */
public class ConnectionWindowEvent extends EventObject {
    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException if source is null.
     */
    public ConnectionWindowEvent(JDialog source) {
        super(source);
    }
}
