package Observers;

import javax.swing.*;
import java.util.EventObject;

/**
 * Created by aagombar on 24/03/14.
 */
public class MainMenubarEvent extends EventObject {
    private static final long serialVersionUID = 6912195748070009662L;

    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException if source is null.
     */
    public MainMenubarEvent(JMenuItem source) {
        super(source);
    }
}
