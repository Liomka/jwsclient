import Observers.IMainMenubarListener;
import Observers.MainMenubarEvent;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

/**
 * Created by aagombar on 24/03/14.
 */
public class MainMenuBar extends JMenuBar {

    private static final long serialVersionUID = 670873855550353046L;
    private JMenuItem _connectToServerItem;
    private JMenuItem _disconnectFromServerItem;
    private JMenuItem _quitItem;

    public MainMenuBar() {
        super();
        add(fileMenu());
    }

    // MENUS
    private JMenu fileMenu() {
        JMenu menu = new JMenu("Fichier");

        menu.add(connectToServerItem());
        menu.add(disconnectFromServerItem());
        menu.addSeparator();
        menu.add(quitItem());

        return menu;
    }

    // ITEMS
    private JMenuItem connectToServerItem() {
        if (_connectToServerItem == null) {
            _connectToServerItem = new JMenuItem("Connect to ...");
            _connectToServerItem.setActionCommand("connectToServer");

            _connectToServerItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    menuClicked(_connectToServerItem);
                }
            });
        }
        return _connectToServerItem;
    }

    private JMenuItem disconnectFromServerItem() {
        if (_disconnectFromServerItem == null) {
            _disconnectFromServerItem = new JMenuItem("Disconnect");
            _disconnectFromServerItem.setActionCommand("disconnectFromServer");
            _disconnectFromServerItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    menuClicked(_disconnectFromServerItem);
                }
            });
        }
        return _disconnectFromServerItem;
    }

    private JMenuItem quitItem() {
        if (_quitItem == null) {
            _quitItem = new JMenuItem("Quit");
            _quitItem.setActionCommand("quit");

            _quitItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    menuClicked(_quitItem);
                }
            });
        }
        return _quitItem;
    }

    // PUBLIC LISTENERS
    private Vector<IMainMenubarListener> _itemsActionListenerList = new Vector<IMainMenubarListener>();
    public void addItemsListener(IMainMenubarListener listener) {
        if (listener != null)
            _itemsActionListenerList.addElement(listener);
    }
    public void removeItemsListener(IMainMenubarListener listener) {
        if (listener != null)
            _itemsActionListenerList.removeElement(listener);
    }

    private void menuClicked(JMenuItem client) {
        MainMenubarEvent clientEvent = new MainMenubarEvent(client);
        for (IMainMenubarListener listener : _itemsActionListenerList)
            listener.menuClicked(clientEvent);
    }
}
