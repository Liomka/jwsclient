import Observers.ConnectionWindowEvent;
import Observers.IConnectionWindowListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

/**
 * Created by aagombar on 27/03/14.
 */
public class ConnectionWindow extends JDialog {

    public static final int DEFAULTPORT = 4512;

    public ConnectionWindow() {
        // WINDOW POSITION
        int width = 220;
        int height = 130;

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        Rectangle windowPosition = new Rectangle(width, height);
        windowPosition.setLocation((Toolkit.getDefaultToolkit().getScreenSize().width  / 2) - (width  / 2),
                (Toolkit.getDefaultToolkit().getScreenSize().height / 2) - (height / 2));
        setBounds(windowPosition);

        // WINDOW CONTENT
        getContentPane().setLayout(new GridLayout(4, 2));

        getContentPane().add(new JLabel("Ip"));
        getContentPane().add(ipField());
        getContentPane().add(new JLabel("Port"));
        getContentPane().add(portField());
        getContentPane().add(new JLabel("Nom"));
        getContentPane().add(nameField());
        getContentPane().add(new JButton("Annuler"));
        getContentPane().add(connectButton());

        setModal(true);
    }

    private ActionListener acceptActionListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            connect();
        }
    };

    private JTextField _ipField;
    private JTextField ipField() {
        if (_ipField == null) {
            _ipField = new JTextField();
            _ipField.setText("Salustre");
            _ipField.addActionListener(acceptActionListener);
        }
        return _ipField;
    }

    private JTextField _portField;
    private JTextField portField() {
        if (_portField == null) {
            _portField = new JTextField();
            _portField.setText(DEFAULTPORT + "");
            _portField.addActionListener(acceptActionListener);
        }
        return _portField;
    }

    private JTextField _nameField;
    private JTextField nameField() {
        if (_nameField == null) {
            _nameField = new JTextField();
            _nameField.addActionListener(acceptActionListener);
        }
        return _nameField;
    }

    private JButton _abortButton;
    private JButton abortButton() {
        if (_abortButton == null) {
            _abortButton = new JButton("Annuler");
            _abortButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    abort();
                }
            });
        }
        return _abortButton;
    }

    private JButton _connectButton;
    private JButton connectButton() {
        if (_connectButton == null) {
            _connectButton = new JButton("Connecter");
            _connectButton.addActionListener(acceptActionListener);
        }
        return _connectButton;
    }

    // PUBLIC LISTENERS
    private Vector<IConnectionWindowListener> _listeners = new Vector<IConnectionWindowListener>();
    public void addConnectionWindowListener(IConnectionWindowListener listener) {
        if (listener != null)
            _listeners.addElement(listener);
    }
    public void removeConnectionWindowListener(IConnectionWindowListener listener) {
        if (listener != null)
            _listeners.removeElement(listener);
    }

    private void connect() {
        dispose();
        ConnectionWindowEvent event = new ConnectionWindowEvent(this);
        for (IConnectionWindowListener listener : _listeners)
            listener.connect(event, ipField().getText(), Integer.parseInt(portField().getText()), nameField().getText());
    }

    private void abort() {
        dispose();
        ConnectionWindowEvent event = new ConnectionWindowEvent(this);
        for (IConnectionWindowListener listener : _listeners)
            listener.abort(event);
    }
}
