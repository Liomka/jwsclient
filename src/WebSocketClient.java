import javax.websocket.*;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * Created by Raymond Barre on 30/04/2014.
 */
@ClientEndpoint
public class WebSocketClient {

    private Session _session;

    private Logger logger = Logger.getLogger(this.getClass().getName());

    @OnOpen
    public void onOpen(Session session) throws IOException {
        logger.info("Connected as: " + session.getId());

        session.getBasicRemote().sendText("/nick bot"); // Set default name

        _session = session;
    }

    /*
    @OnMessage
    public void onMessage(String msg) {
        logger.info("Got msg: " + msg);
    }*/

    @OnClose
    public void onClose(Session session, CloseReason closeReason) {
        logger.info("Disconnect: " + session.getId());
    }

    public void speak(String message) throws IOException {
        getSession().getBasicRemote().sendText(message);
    }

    public void close() throws IOException {
        getSession().close();
    }

    protected Session getSession() {
        return _session;
    }
}